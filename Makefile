VERSION := $(shell sed -n "s/.*VERSION.*= \{1,\}\(.*\)/\1/p;" src/handler.lua)
NAME := $(shell basename $${PWD})
UID := $(shell id -u)
GID := $(shell id -g)
SUMMARY := $(shell sed -n '/^summary: /s/^summary: //p' README.md)
export UID GID NAME VERSION

build: rockspec validate
	@find . -type f -iname "*lua~" -exec rm -f {} \;
	@docker run --rm \
          -v ${PWD}:/plugin \
	  kong /bin/sh -c "apk add --no-cache zip > /dev/null 2>&1 ; cd /plugin ; luarocks make > /dev/null 2>&1 ; luarocks pack ${NAME} 2> /dev/null ; chown ${UID}:${GID} *.rock"
	@mkdir -p dist
	@mv *.rock dist/
	@printf '\n\n Check "dist" folder \n\n'

validate:
	@if [ -z "$${VERSION}" ]; then \
	  printf "\n\nNo VERSION found in handler.lua;\nPlease set it in your object that extends the base_plugin.\nEx: plugin.VERSION = \"0.1.0-1\"\n\n"; \
	  exit 1 ;\
	else \
	  echo ${VERSION} | egrep '(\w.+)-([0-9]+)$$' > /dev/null 2>&1 ; \
	  if [ $${?} -ne 0 ]; then \
  	    printf "\n\nVERSION must follow the pattern [%%w.]+-[%%d]+\nWhich means: 0.0-0 or 0.0.0-0 or ...\nReceived: $${VERSION} \n\n"; \
	    exit 2 ; \
	  fi ; \
	fi
	@if [ -z "${SUMMARY}" ]; then \
  	  printf "\n\nNo SUMMARY found.\nPlease, create a 'README.md' file and place your summary there.\nFollow the pattern '^summary: '\nDo not use double quotes"; \
	  printf "\nExample:\nsummary: this is my summary\n\n\n" ;\
	  exit 4 ;\
	fi
	@if [ ! -f ${NAME}-${VERSION}.rockspec ]; then \
	  make rockspec; \
	fi

copy-docker-compose:
	@[ ! -f docker-compose.yaml ] && cp ../docker-compose.yaml . || printf ''

rockspec:
	@printf 'package = "%s"\nversion = "%s"\n\nsource = {\n url    = "git@bitbucket.org:leandro-carneiro/kong-sqs-plugin.git",\n branch = "master"\n}\n\ndescription = {\n  summary = "%s",\n}\n\ndependencies = {\n  "lua ~> 5.1"\n}\n\nbuild = {\n  type = "builtin",\n  modules = {\n' "${NAME}" "${VERSION}" "${SUMMARY}" > ${NAME}-${VERSION}.rockspec
	@find src -type f -iname "*.lua" -exec bash -c 'printf "    [\"kong.plugins.%s.%s\"] = \"%s\",\n" "${NAME}" "$$(basename $${1/\.lua})" "{}"' _ {} \;	>> ${NAME}-${VERSION}.rockspec
	@printf "  }\n}" >> ${NAME}-${VERSION}.rockspec

clean: copy-docker-compose
	@rm -rf *.rock *.rockspec dist shm
	@find . -type f -iname "*lua~" -exec rm -f {} \;
	@docker-compose down -v

start: validate copy-docker-compose
	@docker-compose up -d

stop: copy-docker-compose
	@docker-compose down

logs: kong-logs
kong-logs:
	@docker logs -f $$(docker ps -qf name=${NAME}_kong_1) 2>&1 || true

shell: kong-bash
kong-bash:
	@docker exec -it $$(docker ps -qf name=${NAME}_kong_1) bash || true

reload: kong-reload
kong-reload:
	@docker exec -it $$(docker ps -qf name=${NAME}_kong_1) bash -c "/usr/local/bin/kong reload"

reconfigure: clean start kong-logs

config:
	@curl -s -X POST http://localhost:8001/services/ -d 'name=metadata-mock' -d url=http://localhost
	@curl -s -X POST http://localhost:8001/services/metadata-mock/routes -d 'paths[]=/latest/meta-data/iam/security-credentials/my-iam-role'
	@curl -i -X POST http://localhost:8001/services/metadata-mock/plugins -F "name=request-termination"  -F 'config.status_code=200' -F 'config.content_type=application/json'  -F 'config.body={  "Code" : "Success",  "LastUpdated" : "2019-04-17T22:50:51Z",  "Type" : "AWS-HMAC",  "AccessKeyId" : "ASIA2YGQJWZG3SR6HGFG",  "SecretAccessKey" : "FFlIUNtACJDrNyRXG8MdtDKAfoKpsRlxSatQ9Fr4",  "Token" : "AgoJb3JpZ2luX2VjEF8aCXNhLWVhc3QtMSJGMEQCIFXw1hTOpVrOry0JtyJJCIn5DEEdRTli3VAKh/VKrNqfAiA78g/aavZBqmAwfmMM/cId2UochwW9KaSocdZeHjYmuCraAwg4EAEaDDczOTE3MTIxOTAyMSIMk7NTwy9peYouk0DLKrcDG0rXrBvBbwowZgGlOg9CpCP7aHgj9+QhU//XjOo5fgD2+OpFJQMMJt+1RdMoF7k6Ws5h/WTOhAsYhrEdyaC16zLRrhjHGtlbpHpocOoQUDp2QpeBcTJ2Edz9zaorYxgRm39KXO+sqEr7Ky2qfti/bOkiZYBRV5QmqUelccb5tB/hbhOlM+lj9FtWj7N9V3aXNJW5H5jb4GpbC2BaxaA3QH8Kw1b4ynh8yL+fDf25+EVbi/lNFVyCj8p5rh+jkLyo2Z2X7rLjwNRsWsDyujWg1kUGwxjuYHFcf78rp3vED62SChx5nxIp+jJghpeD9ggTACA2+Tq/f4fQZs73k7yFfjlM/IkQUyMHcuKsaj3kssZoOHGBhaxnSzIQ6Tc4A3CIswhfD9yxZW+lTXUK1fkzwrYSDT+JBn63tuKgJ92t0lbzbaRIdj/ama/hp9lWT8NlX4SPq9UPLo/cSJBofSyth5wDog/XtmIO6ANZ4/IXh14q19LkeJpBE2t8MgAnimxOeZf8b0OZmCAPeE2tDy/R3E5B5GKzye2uUdtr8rHihed8htnfeMszsIm08ZDneOTZl88hHbR5hjCE3N7lBTq1AWFlKbqTu+NHivsDfDWGvio7OfISQpJkB1u3kC6CLDLvwLthe8YkZfJjk6atJReC7XMsES+eS4ujU3+vAcvutTqkZ8Zx0vew0LLMgwyA84X2ZkJGlgPrUg8bL8nT6AyLS2nOAJlSSP2L2Ep+HZHdSidKJyIM5KHoFiOsUlFZMoXVzY5xYdD8tOyyyYhVaJ7QhbKa7ovxc7KJ2kcJv/9VU448dFZyF0gVqrdxkllLKd7SdnN8k50=",  "Expiration" : "2019-04-18T04:53:36Z"}'
	@curl -s -X POST http://localhost:8001/services/ -d 'name=sqs' -d url=http://localhost
	@curl -s -X POST http://localhost:8001/services/sqs/routes -d 'paths[]=/sqs'
	@curl -s -X POST http://localhost:8001/services/sqs/plugins --data "name=${NAME}" --data-urlencode "config.aws_ami_role=my-iam-role" --data "config.aws_region=sa-east-1" --data "config.queue_name=testegrella" --data "config.aws_metadata_url=http://localhost:8000/latest/meta-data/iam/security-credentials/" --data "config.aws_account_id=739171219021"


config-aux:
	@[ ! -f aux.lua ] && echo -e 'ngx.say("hello from aux - edit aux.lua and run make patch-aux")\nngx.exit(200)' > aux.lua || printf ''
	@curl -s -X POST http://localhost:8001/services/ -d 'name=aux' -d url=http://localhost
	@curl -s -X POST http://localhost:8001/services/aux/routes -d 'paths[]=/aux'
	@curl -i -X POST http://localhost:8001/services/aux/plugins -F "name=pre-function" -F "config.functions=@aux.lua"

patch-aux:
	@curl -i -X PATCH http://localhost:8001/plugins/$$(curl -s http://localhost:8001/plugins/ | jq -r ".data[] |  select (.name|test(\"pre-function\")) .id")      -F "name=pre-function"      -F "config.functions=@aux.lua"
	@echo " "

req-aux:
	@curl -s http://localhost:8000/aux

